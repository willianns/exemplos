﻿using System;
using HtmlAgilityPack;
using System.ComponentModel.DataAnnotations;

namespace ValidandoConteudoHtml.Suporte
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class InspectHtmlAttribute : ValidationAttribute
    {
        /// <summary>
        /// Delimite as tags por virgula (BlackList="script,iframe,frame")
        /// </summary>
        public string BlackList { get; set; }

        public override bool IsValid(object value)
        {
            return ValidaHtml(Convert.ToString(value));
        }

        private bool ValidaHtml(string Html)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(Html);

            foreach (var tag in BlackList.Split(','))
            {
                var node = doc.DocumentNode.SelectSingleNode(String.Concat(@"//", tag.Trim()));
                if (node != null)
                {
                    ErrorMessage = String.Format("Foi encontrado uma tag não permitida em seu HTML => tag[{0}] ",
                                                 node.Name);
                    return false;
                }
            }

            return true;
        }
    }


}