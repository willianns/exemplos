﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using ValidandoConteudoHtml.Suporte;

namespace ValidandoConteudoHtml.ViewModels
{
    public class PostEditor
    {
        [Required]
        public string Titulo { get; set; }

        [DataType(DataType.MultilineText),
         AllowHtml,
         InspectHtml(BlackList = "script,iframe,frame,style,link")]
        public string Conteudo { get; set; }
    }
}