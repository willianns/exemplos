﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ValidandoConteudoHtml.ViewModels;

namespace ValidandoConteudoHtml.Controllers
{
    public class PostsController : Controller
    {
        public ActionResult Index()
        {
            return View(TempData["model"]);
        }

        public ActionResult Novo()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Novo(PostEditor postEditor)
        {
            if (!ModelState.IsValid)
                return View();

            TempData["model"] = postEditor;

            return RedirectToAction("Index");
        }
    }
}
