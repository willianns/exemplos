﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoFacSamples
{
    public interface IFormatter
    {
        string Output(string message);
    }
}
