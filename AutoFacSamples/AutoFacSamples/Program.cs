﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoFacSamples
{
    using Autofac;
    class Program
    {
        static void Main(string[] args)
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.Register(c => new XmlFormatter()).As<IFormatter>();
            builder.Register(c => new Logger(c.Resolve<IFormatter>())).AsSelf();
            IContainer container = builder.Build();

            Logger xmlLogger = container.Resolve<Logger>();
            xmlLogger.Log("everything went better than expected!");

            Console.ReadKey();
        }
    }
}
