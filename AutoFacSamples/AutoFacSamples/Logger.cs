﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoFacSamples
{
    public class Logger
    {
        readonly IFormatter _formatter;

        public Logger(IFormatter formatter)
        {
            _formatter = formatter;
        }

        public void Log(string message)
        {
            Console.WriteLine(_formatter.Output(message));
        }
    }
}
