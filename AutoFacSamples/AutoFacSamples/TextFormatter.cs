﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoFacSamples
{
    public class TextFormatter : IFormatter
    {
        public string Output(string message)
        {
            return String.Format("message: {0} ", message);
        }
    }
}
